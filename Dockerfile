FROM golang:1.20.11-alpine3.18 AS build

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o /subgen . 

FROM alpine:3.18
COPY --from=build /subgen /subgen
CMD ["/subgen"]
